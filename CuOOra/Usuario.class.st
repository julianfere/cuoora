"
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design:

For the Class part:  State a one line summary. For example, ""I represent a paragraph of text"".
a
For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.


    Implementation Points
"
Class {
	#name : #Usuario,
	#superclass : #Object,
	#instVars : [
		'userName',
		'password',
		'fecha',
		'seguidos',
		'preguntas',
		'respuestas',
		'topicos'
	],
	#category : #'CuOOra-Model'
}

{ #category : #'as yet unclassified' }
Usuario class >> nuevoConUserName: anUserName password: aPassword [
 ^ self new inicializarConUserName: anUserName password: aPassword 
]

{ #category : #'add-remove' }
Usuario >> agregarPregunta: unaPregunta [
	preguntas add: unaPregunta.
]

{ #category : #'add-remove' }
Usuario >> agregarRespuesta: unaRespuesta [ 
	respuestas add: unaRespuesta .
]

{ #category : #'add-remove' }
Usuario >> agregarTopicosDeInteres: unosTopicos [
	topicos addAll: unosTopicos
]

{ #category : #public }
Usuario >> calcularPuntaje [
	^ (preguntas sumNumbers: [ :each | each calcularPuntaje ])
		+ (self filtrarRespuestasPropias
		sumNumbers: [ :each | each calcularPuntaje ])
]

{ #category : #accessing }
Usuario >> chequearSeguidosConAutor: unAutor [
	^ seguidos anySatisfy: [ :each | each compararConAutor: unAutor ]
]

{ #category : #accessing }
Usuario >> compararConAutor: unAutor [
	^ self = unAutor
]

{ #category : #'add-remove' }
Usuario >> eliminarPregunta: unaPregunta [ 
	preguntas remove: unaPregunta 
]

{ #category : #'add-remove' }
Usuario >> eliminarRespuesta: unaRespuesta [
	respuestas remove: unaRespuesta 
]

{ #category : #accessing }
Usuario >> fechaYHora [
	^ fecha
]

{ #category : #accessing }
Usuario >> fechaYHora: anObject [
	fecha := anObject
]

{ #category : #'as yet unclassified' }
Usuario >> filtrarRespuestasPropias [
^ respuestas reject:[:each | each verificarAutor: self]
]

{ #category : #initialization }
Usuario >> inicializarConUserName: unUserName password: unPassword [
	userName := unUserName.
	password := unPassword
]

{ #category : #initialization }
Usuario >> initialize [
	fecha := Date today.
	topicos := (Topico
		nuevoConNombre: '#€~@'
		descripcion: 'Sin descripcion') asOrderedCollection.
	seguidos := Set new.
	preguntas := OrderedCollection new.
	respuestas := OrderedCollection new
]

{ #category : #accessing }
Usuario >> password [
	^ password
]

{ #category : #accessing }
Usuario >> password: anObject [
	password := anObject
]

{ #category : #accessing }
Usuario >> preguntas [
	^ preguntas
]

{ #category : #public }
Usuario >> puedoSeguir: unUsuario [
	unUsuario  == self
		ifTrue: [ ^ false ].
	(seguidos includes: unUsuario )
		ifTrue: [ ^ false ].
	^ true
	
]

{ #category : #accessing }
Usuario >> respuestas [
	^ respuestas
]

{ #category : #accessing }
Usuario >> seguidos [
	^ seguidos
]

{ #category : #'add-remove' }
Usuario >> seguirUsuario: unUsuario [
	self == unUsuario ifFalse:[seguidos add: unUsuario]
]

{ #category : #accessing }
Usuario >> topicos [
	^ topicos
]

{ #category : #accessing }
Usuario >> userName [
	^ userName 
]
