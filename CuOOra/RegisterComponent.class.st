"
For example, ""I represent a paragraph of text"".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	name:		<Object>


    Implementation Points
"
Class {
	#name : #RegisterComponent,
	#superclass : #AccessGrantingComponent,
	#category : #'CuOOra-UI'
}

{ #category : #'as yet unclassified' }
RegisterComponent >> register [
	self model agregarUsuario: (Usuario nuevoConUserName: name password: password ).
	self answer
]

{ #category : #'as yet unclassified' }
RegisterComponent >> renderButtonsOn: aCanvas [ 
	self renderSignUpButtonOn: aCanvas
]

{ #category : #rendering }
RegisterComponent >> renderContentOn: aCanvas [
	aCanvas
		form: [ self renderInputElementsOn: aCanvas.
			self renderButtonsOn: aCanvas ]
]

{ #category : #'as yet unclassified' }
RegisterComponent >> renderInputElementsOn: aCanvas [
	super renderInputElementsOn: aCanvas
]

{ #category : #'as yet unclassified' }
RegisterComponent >> renderSignUpButtonOn: aCanvas [
	aCanvas submitButton
		callback: [ self register ];
		with: 'Register'
]
