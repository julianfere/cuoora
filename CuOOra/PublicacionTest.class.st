Class {
	#name : #PublicacionTest,
	#superclass : #TestCase,
	#instVars : [
		'p1',
		'p2',
		'p3',
		'p4',
		'usuario1',
		'usuario2',
		'usuario3',
		'topicos'
	],
	#category : #'CuOOra-Test'
}

{ #category : #running }
PublicacionTest >> setUp [
	topicos := (Topico nuevoConNombre: '1' descripcion: '1')
		asOrderedCollection.
	usuario1 := Usuario nuevoConUserName: 'Fere' password: 'test'.
	usuario2 := Usuario nuevoConUserName: 'Fere2' password: 'test2'.
	usuario3 := Usuario nuevoConUserName: 'Fere3' password: 'test3'.
	p1 := Pregunta
		nuevoConTitulo: 'preg1'
		autor: usuario2
		cuerpo: 'asd'
		topicos: topicos.
	p2 := Pregunta
		nuevoConTitulo: 'preg2'
		autor: usuario3
		cuerpo: 'asd'
		topicos: topicos.
	p3 := Pregunta
		nuevoConTitulo: 'preg2'
		autor: usuario1
		cuerpo: 'asd'
		topicos: topicos.
	p4:= Pregunta
		nuevoConTitulo: 'preg2'
		autor: usuario1
		cuerpo: 'asd'
		topicos: topicos.
	p3 darLike: usuario2 ; darLike:usuario1 ; darDislike: usuario3.
]

{ #category : #tests }
PublicacionTest >> testAddReaccion [
	self assert: p1 reacciones size equals: 0.
	p1 darLike: usuario1.
	p1 darDislike: usuario2.
	self assert: p1 reacciones size equals: 2
]

{ #category : #tests }
PublicacionTest >> testAgregarReaccionUsuario [
	self assert: p1 reacciones size equals: 0.
	p1 darLike: usuario1.
	p1 darDislike: usuario2.
	self assert: p1 reacciones size equals: 2
]

{ #category : #tests }
PublicacionTest >> testCalcularDiferenciaEntreLikesYDislikes [
|p3 usuario3|
usuario3:= Usuario nuevoConUserName: 'Raul' password: '1234'.
p3 := Pregunta
		nuevoConTitulo: 'preg3'
		autor: 'a'
		cuerpo: 'asd'
		topicos: topicos.
p3 darLike: usuario1 ; darLike: usuario2; darDislike: usuario3.
self assert: p3 calcularDiferenciaEntreLikesYDislikes equals: 1



]

{ #category : #tests }
PublicacionTest >> testCambiarEstadoDeUsuario [
	p1 darDislike: usuario1.
	self assert: ((p1 reacciones) at:1) estado equals: false.
	p1 darLike: usuario1.
	self assert: ((p1 reacciones) at:1) estado equals: true.
	

]

{ #category : #tests }
PublicacionTest >> testCambiarEstadoDeUsuarioEstado [
	| dislike likes |
	p1 darLike: usuario1.
	self assert: p1 cantidadDeLikes equals: 1.
	p1 darDislike: usuario1.
	likes := p1 cantidadDeLikes .
	dislike := p1 cantidadDeDislikes.
	self deny: likes equals: dislike.

]

{ #category : #tests }
PublicacionTest >> testCantidadDeDislikes [
self assert: p3 cantidadDeDislikes equals: 1.
self assert: p4 cantidadDeDislikes equals: 0
	
]

{ #category : #tests }
PublicacionTest >> testCantidadDeLikes [
	self assert: p3 cantidadDeLikes equals: 2.
	self assert: p4 cantidadDeLikes equals: 0.
	
]

{ #category : #tests }
PublicacionTest >> testDarDislike [
	p2 darDislike: usuario2.
	self assert: ((p2 reacciones) at:1) estado equals: false. 
	p2 darDislike: usuario2.
	self assert: ((p2 reacciones) size) equals: 0.
	
]

{ #category : #tests }
PublicacionTest >> testDarLike [
	p1 darLike: usuario1.
	self assert: ((p1 reacciones) at:1) estado equals: true. 
	p1 darLike: usuario1.
	self assert: ((p1 reacciones) size) equals: 0.
]

{ #category : #tests }
PublicacionTest >> testQuitarReaccion [
	| cantLikes |
	p1 darLike: usuario1.
	cantLikes := p1 cantidadDeLikes .
	p1 darLike: usuario1.
	self deny: cantLikes equals: p1 cantidadDeDislikes .
	
]
