Class {
	#name : #UsuarioTest,
	#superclass : #TestCase,
	#instVars : [
		'u1',
		'u2',
		'u3',
		'u4',
		'u5',
		'topicos',
		'pregunta1',
		'pregunta2',
		'pregunta3',
		'respuesta1',
		'respuesta2',
		'respuesta3'
	],
	#category : #'CuOOra-Test'
}

{ #category : #tests }
UsuarioTest >> setUp [
	u1 := Usuario nuevoConUserName: 'u1' password: '1'.
	u2 := Usuario nuevoConUserName: 'u2' password: '2'.
	u3 := Usuario nuevoConUserName: 'u3' password: '3'.
	u4 := Usuario nuevoConUserName: 'u4' password: '4'.
	u5 := Usuario nuevoConUserName: 'u5' password: '5'.
	u1 seguirUsuario: u2.
	topicos := (Topico nuevoConNombre: '1' descripcion: '1')
		asOrderedCollection.
	pregunta1 := Pregunta
		nuevoConTitulo: 'hola'
		autor: u1
		cuerpo: 'asd'
		topicos: topicos.
	pregunta2 := Pregunta
		nuevoConTitulo: 'hola'
		autor: u1
		cuerpo: 'asd'
		topicos: topicos.
	pregunta3 := Pregunta
		nuevoConTitulo: 'hola'
		autor: u4
		cuerpo: 'asd'
		topicos: topicos.
	respuesta1 := Respuesta nuevoConCuerpo: 'asd' autor: u1.
	respuesta2 := Respuesta nuevoConCuerpo: 'asd' autor: u2.
	respuesta3 := Respuesta nuevoConCuerpo: 'asd' autor: u4.
	respuesta1
		darLike: u1;
		darDislike: u2.
	respuesta2
		darLike: u1;
		darLike: u2.
	pregunta1
		darLike: u1;
		darLike: u3;
		darDislike: u2;
		agregarRespuesta: respuesta1;
		agregarRespuesta: respuesta2.
	pregunta2 darLike: u1.
	u1
		agregarPregunta: pregunta1;
		agregarPregunta: pregunta2;
		agregarRespuesta: respuesta1;
		agregarRespuesta: respuesta2.
	u4 agregarPregunta: pregunta3.
	u5 agregarRespuesta: respuesta3
]

{ #category : #tests }
UsuarioTest >> testAgregarPregunta [
	| user|
	user := Usuario nuevoConUserName: 'test' password: 123.
	user agregarPregunta: pregunta1.
	self assert: user preguntas size equals: 1.
	self assert: (user preguntas includes: pregunta1 )
]

{ #category : #tests }
UsuarioTest >> testAgregarRespuesta [
	| user|
	user := Usuario nuevoConUserName: 'test' password: 123.
	user agregarRespuesta: respuesta1.
	self assert: user respuestas size equals: 1.
	self assert: (user respuestas includes: respuesta1 )

]

{ #category : #tests }
UsuarioTest >> testAgregarTopicosDeInteres [
	| t1 user topicosTest topicCount |
	user := Usuario nuevoConUserName: 'test' password: 123.
	topicCount := user topicos size.
	t1 := Topico nuevoConNombre: '123' descripcion: 123.
	topicosTest := t1 asOrderedCollection.
	user agregarTopicosDeInteres: topicosTest.
	self deny: user topicos size equals: topicCount.
	self assert: (user topicos includes: t1)
]

{ #category : #tests }
UsuarioTest >> testCalcularPuntaje [
	self assert: u1 calcularPuntaje equals: 94.
	self assert: u3 calcularPuntaje equals: 0.
	self assert: u4 calcularPuntaje equals: 20.
	self assert: u5 calcularPuntaje  equals: 50.
]

{ #category : #tests }
UsuarioTest >> testChequearSeguidosConAutor [
	self assert: (u1 chequearSeguidosConAutor: u2).
	self deny: (u1 chequearSeguidosConAutor: u5)
]

{ #category : #tests }
UsuarioTest >> testCompararConAutor [
	self assert: (u1 compararConAutor: u1).
	self deny: (u1 compararConAutor: u2).
]

{ #category : #tests }
UsuarioTest >> testEliminarPregunta [
	| cantidadDePreguntas |
	cantidadDePreguntas := u1 preguntas size.
	u1 eliminarPregunta: pregunta1.
	self deny: cantidadDePreguntas equals: u1 preguntas size.
	self deny: (u1 preguntas includes: pregunta1)
]

{ #category : #tests }
UsuarioTest >> testEliminarRespuesta [
	| cantidadDeRespuestas |
	cantidadDeRespuestas := u1 respuestas size.
	u1 eliminarRespuesta: respuesta1.
	self deny: cantidadDeRespuestas equals: u1 respuestas size.
	self deny: (u1 respuestas includes: respuesta1)
]

{ #category : #tests }
UsuarioTest >> testFiltrarRespuestasPropias [
	self
		assert: u1 filtrarRespuestasPropias
		equals: respuesta2 asOrderedCollection.
	self
		assert: u3 filtrarRespuestasPropias
		equals: OrderedCollection new.
]

{ #category : #tests }
UsuarioTest >> testPuedoSeguir [
 	self assert: (u1 puedoSeguir: u3).
	u1 seguirUsuario: u3.
	self deny: (u1 puedoSeguir: u3).
	self deny: (u1 puedoSeguir: u1).


]

{ #category : #tests }
UsuarioTest >> testSeguirUsuario [
	| followers |
	followers := u2 seguidos size.
	u2 seguirUsuario: u1.
	self deny: followers equals: u2 seguidos size.
	self assert: (u2 seguidos includes: u1)
	
	
]
