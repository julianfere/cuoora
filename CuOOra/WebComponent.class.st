"
Superclase para reutilizar codigo
"
Class {
	#name : #WebComponent,
	#superclass : #WAComponent,
	#category : #'CuOOra-UI'
}

{ #category : #accessing }
WebComponent >> model [ 
	^ CuOOra soleInstance 
]

{ #category : #rendering }
WebComponent >> renderAuthorOn: aCanvas of: aPost [
	aCanvas div
		class: 'author';
		with: [ aCanvas text: 'Realizada por  '.
			aCanvas anchor
				class: 'user-name';
				callback: [ self call: (UserComponent nuevoConUsuario: aPost autor) ];
				with: aPost nombreAutor ]
]

{ #category : #rendering }
WebComponent >> renderDetailLinkOn: aCanvas of: aPost [
	aCanvas anchor
		callback: [ self call: (PostComponent nuevoConPost: aPost) ];
		with: 'Ver Respuestas'
]

{ #category : #rendering }
WebComponent >> renderDislikeLinkOf: aPublish on: aCanvas [
	 aCanvas anchor
				callback: [ aPublish darDislike: (self session user) ];
				with: 'Dislike'
]

{ #category : #rendering }
WebComponent >> renderHeaderComponentOn: aCanvas [
	aCanvas div
		id: 'header';
		class: 'header-container';
		with: [ aCanvas heading: 'CuOOra'.
			aCanvas div
				id: 'header-item';
				class: 'header-content';
				with: [ aCanvas heading
						level: 2;
						with: self session user userName.
						aCanvas space: 5.
					self renderLogoutButtonOn: aCanvas ] ].
	aCanvas horizontalRule
]

{ #category : #rendering }
WebComponent >> renderLikeLinkOf: aPublish on: aCanvas [
	 aCanvas anchor
				callback: [ aPublish darLike: (self session user) ];
				with: 'Like'
]

{ #category : #rendering }
WebComponent >> renderLogoutButtonOn: aCanvas [
	aCanvas
		paragraph: [ aCanvas anchor
				callback: [ self returnToLoginComponent ];
				with: [ aCanvas button: 'Log out' ] ]
]

{ #category : #rendering }
WebComponent >> renderQuestion: aPublish on: aCanvas [
	aCanvas div
		class: 'question';
		with: [ aCanvas div class:'title-header'; with:[
				aCanvas text: aPublish titulo;
				break.].
			self renderTopicsOf: aPublish on: aCanvas.
			aCanvas 
				text: aPublish cantidadDeLikes asString , '  Likes';
				text: ' | ';
				text: aPublish cantidadDeDislikes asString , '  Dislikes';	
				text: ' | ';
				text: aPublish cantidadDeRespuestas asString , '  Respuestas';			
				break.
			self renderLikeLinkOf: aPublish on: aCanvas.
			aCanvas text: '  |  '.
			self renderDislikeLinkOf: aPublish on: aCanvas.
			aCanvas text: '  |  '.
			self renderDetailLinkOn: aCanvas of: aPublish.
			self renderAuthorOn: aCanvas of: aPublish.
			aCanvas horizontalRule ]
]

{ #category : #rendering }
WebComponent >> renderQuestionWhithoutAuthor: aPublish  on: aCanvas [ 
	aCanvas div
		class: 'question';
		with: [ aCanvas
				strong: aPublish titulo;
				break.
			self renderTopicsOf: aPublish on: aCanvas.
			aCanvas
				text: aPublish cantidadDeRespuestas asString , '  Respuestas';
				text: ' | ';
				text: aPublish cantidadDeLikes asString , '  Likes';
				text: ' | ';
				text: aPublish cantidadDeDislikes asString , '  Dislikes';
				break.
			self renderLikeLinkOf: aPublish on: aCanvas.
			aCanvas text: '  |  '.
			self renderDislikeLinkOf: aPublish on: aCanvas.
			aCanvas text: '  |  '.
			self renderDetailLinkOn: aCanvas of: aPublish.
			aCanvas horizontalRule ]
]

{ #category : #rendering }
WebComponent >> renderQuestionWithoutDetail: aPublish on: aCanvas [
	aCanvas div
		class: 'question';
		with: [ aCanvas
				strong: aPublish titulo;
				break.
			self renderTopicsOf: aPublish on: aCanvas.
			aCanvas
				text: aPublish cantidadDeRespuestas asString , '  Respuestas';
				text: ' | ';
				text: aPublish cantidadDeLikes asString , '  Likes';
				text: ' | ';
				text: aPublish cantidadDeDislikes asString , '  Dislikes';
				break.
			self renderLikeLinkOf: aPublish on: aCanvas.
			aCanvas text: '  |  '.
			self renderDislikeLinkOf: aPublish on: aCanvas.
			self renderAuthorOn: aCanvas of: aPublish   ]
]

{ #category : #rendering }
WebComponent >> renderTopicsOf: aPublish on: aCanvas [
	aCanvas text: 'Topicos: '.
	aPublish topicos
		do: [ :each | aCanvas text: each nombre , ', ' ].
	aCanvas break.
]

{ #category : #callbacks }
WebComponent >> returnToLoginComponent [
	self session unregister. 
   self requestContext redirectTo: self application url.
	"self call: LoginComponent new"
]

{ #category : #hooks }
WebComponent >> style [
	^ '
	.header-container { 
		display: flex;
		justify-content: space-between;
		flex-wrap: wrap;
	 }
	.header-content {
		display: flex;
		align-items:center;
	 }
	.questions { 
		display: flex;
		padding-left: 1.2em;
	 }
	.responses-container { 
		display: flex;
		flex-flow: column wrap;
	 }
	.response { 
		padding-left: 1.2em;
		font-size: 1.2em;
	 }
	.response-options { 
		display: flex;
		flex-flow: column wrap;
		align-items:center;
	 }
	.response-form { 
		display: flex;
	 }
	.submit-options { 
		display: flex;
		flex-wrap: wrap;
		justify-content:center;
	 }
	.submit-item { 
		align-self: center;
	 }
	.user-name { 
		font-weight: bold;
		color: black;
	 }
	.title-header { 
		display:flex;
		align-items:center;
		font-size: 1.3em;
		font-family: Arial, Helvetica, sans-serif;
		font-style: oblique;
	 }
	.user-header { 
		display:flex;
		align-items:center;	
		font-weight: bold;
		font-size: 1.5em;
	 }
'
]
