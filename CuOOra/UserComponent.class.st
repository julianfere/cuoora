"
UserComponent
"
Class {
	#name : #UserComponent,
	#superclass : #WebComponent,
	#instVars : [
		'user'
	],
	#category : #'CuOOra-UI'
}

{ #category : #'as yet unclassified' }
UserComponent class >> nuevoConUsuario: aUser [
	^ self new user: aUser.
]

{ #category : #rendering }
UserComponent >> renderContentOn: aCanvas [ 
	self renderHeaderComponentOn: aCanvas.
	self renderUserInfoOn: aCanvas.
	self renderUserQuestionsOn: aCanvas
]

{ #category : #rendering }
UserComponent >> renderUserInfoOn: aCanvas [
	aCanvas div
		class: 'user-header';
		with: [ aCanvas paragraph with: 'Preguntas de ' , user userName.
			aCanvas space: 3.
			aCanvas anchor
				callback: [ (self session user)seguirUsuario: user  ];
				with: 'Follow' ]
]

{ #category : #rendering }
UserComponent >> renderUserQuestionsOn: aCanvas [
	(self model obtenerPreguntasDeUsuario: user)
		do: [ :preg | self renderQuestionWhithoutAuthor: preg on: aCanvas ]
]

{ #category : #accessing }
UserComponent >> user [
	^ user
]

{ #category : #accessing }
UserComponent >> user: anObject [
	user := anObject
]
