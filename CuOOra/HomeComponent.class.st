"
""Home de la aplicacion""

""codigo a ejecutar en el playgorud para instanciar el caso de prueba""

|application c|
application := WAAdmin register: LoginComponent asApplicationAt: 'cuoora'.

application sessionClass: SessionWithUser.
c:= CuOOra soleInstance.
c loadTestInfo.
"
Class {
	#name : #HomeComponent,
	#superclass : #WebComponent,
	#category : #'CuOOra-UI'
}

{ #category : #callbacks }
HomeComponent >> createNewPost [
	self call: NewPostComponent new
]

{ #category : #rendering }
HomeComponent >> renderContentOn: aCanvas [
	self renderHeaderComponentOn: aCanvas.
	self renderFiverRelevantsQuestionsOn: aCanvas.
	self renderMakeAQuestionButtonOn: aCanvas.
	"self renderPostsOn: aCanvas.
	aCanvas horizontalRule.
	self renderFriendsOn: aCanvas.
	aCanvas horizontalRule"
]

{ #category : #rendering }
HomeComponent >> renderFiverRelevantsQuestionsOn: aCanvas [
	aCanvas heading: 'Ultimas Preguntas'.
	aCanvas unorderedList: [ (self model obtenerUltimasCincoPreguntasRelevantesOrdenadasPorFecha: (self model obtenerPreguntasRelevantesParaUsuario: self session user ))
				do: [ :each | aCanvas listItem: [ self renderQuestion: each on: aCanvas ] ] ].
]

{ #category : #rendering }
HomeComponent >> renderMakeAQuestionButtonOn: aCanvas [
aCanvas
		paragraph: [ aCanvas anchor
				callback: [ self createNewPost  ];
				with: [ aCanvas button with: 'Hacer una pregunta' ] ]
]
