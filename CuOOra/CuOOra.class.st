"
""codigo a ejecutar en el playgorud para instanciar el caso de prueba""

|application c|
application := WAAdmin register: LoginComponent asApplicationAt: 'cuoora'.

application sessionClass: SessionWithUser.
c:= CuOOra soleInstance.
c loadTestInfo.
"
Class {
	#name : #CuOOra,
	#superclass : #Object,
	#instVars : [
		'usuarios',
		'topicos',
		'preguntas'
	],
	#classInstVars : [
		'soleInstance'
	],
	#category : #'CuOOra-Model'
}

{ #category : #accessing }
CuOOra class >> clearSoleInstance [
	soleInstance := nil.
]

{ #category : #'private-creation' }
CuOOra class >> nuevoConPreguntas: unasPreguntas [
	^ self new inicializarConPreguntas: unasPreguntas
]

{ #category : #accessing }
CuOOra class >> soleInstance [ 
	^ soleInstance ifNil: [ soleInstance := self new ]
]

{ #category : #'add-remove' }
CuOOra >> agregarTopico: unTopico [
	topicos add: unTopico
]

{ #category : #'add-remove' }
CuOOra >> agregarUsuario: unUsuario [
	usuarios add: unUsuario.
	^ unUsuario
]

{ #category : #'as yet unclassified' }
CuOOra >> cantidadDeTopicos [
	^ topicos size
	"metodo para tests"
]

{ #category : #'as yet unclassified' }
CuOOra >> cantidadDeUsuarios [
	^ usuarios size.
	
]

{ #category : #'as yet unclassified' }
CuOOra >> cantitadDePreguntas [
	^ preguntas size 
	"Metodo para tests"
]

{ #category : #'add-remove' }
CuOOra >> crearPreguntaConTitulo: unTitulo autor: unAutor cuerpo: unCuerpo topicos: nombresDeTopicos [
	| pregunta preguntaTopicos |
	preguntaTopicos := (nombresDeTopicos
		collect: [ :each | self verificarTopico: each ]).
	pregunta := Pregunta
		nuevoConTitulo: unTitulo
		autor: unAutor
		cuerpo: unCuerpo
		topicos: preguntaTopicos.
	unAutor agregarPregunta: pregunta.
	preguntas add: pregunta.
	^ pregunta
]

{ #category : #'add-remove' }
CuOOra >> eliminarPregunta: unaPregunta [
	unaPregunta eliminar.
	preguntas remove: unaPregunta
]

{ #category : #'private - initialization' }
CuOOra >> inicializarConPreguntas: unasPreguntas [
	preguntas addAll: unasPreguntas
]

{ #category : #initialization }
CuOOra >> initialize [
	usuarios := Set new.
	topicos := Set new.
	preguntas := OrderedCollection new.
]

{ #category : #'testing - initialization' }
CuOOra >> loadTestInfo [
	"METODO PARA INSTANCIAR CASO DE PRUEBA"
	| pedro juan diego topicoOO1 topicoTest topicoSmalltalk topicosPregunta1 topicosPregunta2 pregunta1 pregunta2 |
	"Creacion de usuarios"
	pedro := self agregarUsuario: (Usuario nuevoConUserName: 'pedro@cuoora.com' password: 'pedro@cuoora.com').
		"crearUsuarioConUserName: 'pedro@cuoora.com'
		password: 'pedro@cuoora.com'."
	juan := self agregarUsuario: (Usuario nuevoConUserName: 'juan@cuoora.com' password: 'juan@cuoora.com').
		"crearUsuarioConUserName: 'juan@cuoora.com'
		password: 'juan@cuoora.com'."
	diego := self agregarUsuario: (Usuario nuevoConUserName: 'diego@cuoora.com' password: 'diego@cuoora.com').
		"crearUsuarioConUserName: 'diego@cuoora.com'
		password: 'diego@cuoora.com'."
	"Establer relaciones"
	juan seguirUsuario: diego.
	pedro seguirUsuario: juan.
	"Creacion de topicos"
	topicoOO1 := self
		registrarTopicoConNombre: 'OO1'
		descripcion: 'Topico de objetos 1'.
	topicoTest := self
		registrarTopicoConNombre: 'Test de Unidad'
		descripcion: 'Topico de Test de Unidad'.
	topicoSmalltalk := self
		registrarTopicoConNombre: 'Smalltalk'
		descripcion: 'Topico de Smalltalk'.
	"Creacion de preguntas"
	topicosPregunta1 := #('OO1''Test de unidad') asOrderedCollection.
	pregunta1 := self
		crearPreguntaConTitulo: '¿Para que sirve el metodo SetUp?'
		autor: pedro
		cuerpo: 'Nose para que sirve'
		topicos: topicosPregunta1.
	topicosPregunta2 := #('OO1' 'Smalltalk') asOrderedCollection.
	pregunta2 := self
		crearPreguntaConTitulo: '¿Que significa #messageNotUnderstood?'
		autor: diego
		cuerpo: 'nose que significa'
		topicos: topicosPregunta2.
	"Agregando respuestas"
	pregunta1
		crearRespuestaConCuerpo:
			'Sirve para instanciar los objetos que son
			evaluados por el test en un único método y que se ejecute
			siempre antes de cada test.'
		autor: diego.
	pregunta2
		crearRespuestaConCuerpo:
			'Significa que el objeto que recibió el mensaje
			no encontró ningún método para ejecutar en respuesta'
		autor: pedro
]

{ #category : #accessing }
CuOOra >> obtenerPreguntasDeTopicos: unosTopicos [
	^ preguntas select: [ :each | each chequearTopicos: unosTopicos ]
]

{ #category : #public }
CuOOra >> obtenerPreguntasDeUsuario: unUsuario [
	^ preguntas select: [ :each | each compararCon: unUsuario ]
]

{ #category : #accessing }
CuOOra >> obtenerPreguntasRelevantesParaUsuario: unUsuario [
	^ preguntas
		select: [ :each | each obtenerPreguntasRelevantesParaUsuario: unUsuario ]
]

{ #category : #accessing }
CuOOra >> obtenerUltimasCincoPreguntasRelevantesOrdenadasPorFecha: preguntasRelevantes [
	| preguntasOrdenadas |
	preguntasOrdenadas := self
		ordenarPorFechaDeMayorAMenor: preguntasRelevantes.
	preguntasRelevantes size >= 5
		ifTrue: [ ^ preguntasOrdenadas first: 5 ]
		ifFalse: [ ^ preguntasOrdenadas ]
]

{ #category : #private }
CuOOra >> ordenarPorFechaDeMayorAMenor: preguntasRelevantes [
	^ preguntasRelevantes sort: [ :a :b | a fechaYHora >= b fechaYHora ]
]

{ #category : #accessing }
CuOOra >> preguntas [ 
	^ preguntas
]

{ #category : #'add-remove' }
CuOOra >> registrarTopicoConNombre: unNombre descripcion: unaDescripcion [
	self agregarTopico: (Topico nuevoConNombre: unNombre descripcion: unaDescripcion)
 
]

{ #category : #accessing }
CuOOra >> topicos [
	 ^ topicos 
]

{ #category : #public }
CuOOra >> usuarioConNombre: unNombre protegidoPor: unaPassword [
	^ usuarios
		detect:
			[ :usuario | usuario userName = unNombre & (usuario password = unaPassword) ]
]

{ #category : #accessing }
CuOOra >> usuarios [
	^ usuarios
]

{ #category : #private }
CuOOra >> verificarTopico: nombreTopico [
	| topico |
	topicos
		detect: [ :each | each nombre = nombreTopico ]
		ifFound: [ :each | ^ each ]
		ifNone: [ self
				agregarTopico:
					(topico := Topico
						nuevoConNombre: nombreTopico
						descripcion: 'Sin Descripcion').
			^ topico ]
]
