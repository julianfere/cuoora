"
componente para pregunta
"
Class {
	#name : #PostComponent,
	#superclass : #WebComponent,
	#instVars : [
		'post'
	],
	#category : #'CuOOra-UI'
}

{ #category : #'as yet unclassified' }
PostComponent class >> nuevoConPost: aPost [
	^ self new post: aPost
]

{ #category : #accessing }
PostComponent >> post [
	^ post
]

{ #category : #accessing }
PostComponent >> post: anObject [
	post := anObject
]

{ #category : #rendering }
PostComponent >> renderContentOn: aCanvas [
	self renderHeaderComponentOn: aCanvas.
	self renderQuestionDetailOn: aCanvas.
	self renderResponsesOn: aCanvas.
	self renderOptionOn: aCanvas
]

{ #category : #rendering }
PostComponent >> renderOptionOn: aCanvas [
	aCanvas div
		class: 'response-options';
		with: [aCanvas break; text: 'No hay mas respuestas'.
			aCanvas paragraph:[aCanvas anchor
				callback: [ self call: (NewResponseComponent nuevoConPost: post) ];
				with: [aCanvas button with:'Agregar Respuesta'] ]]
]

{ #category : #rendering }
PostComponent >> renderQuestionDetailOn: aCanvas [
	self renderQuestionWithoutDetail: post on: aCanvas.
	aCanvas horizontalRule 
]

{ #category : #rendering }
PostComponent >> renderQuestionResponseOn: aCanvas response: aResponse [
	aCanvas div
		id: 'response';
		class: 'response';
		with: [aCanvas 
				text: aResponse cuerpo ;
				break;
				text: aResponse cantidadDeLikes asString , '  Likes';
				text: ' | ';
				text: aResponse cantidadDeDislikes asString , '  Dislikes';
				break.
			self renderLikeLinkOf: aResponse on: aCanvas.
			aCanvas text: '  |  '.
			self renderDislikeLinkOf: aResponse on: aCanvas.
			self renderAuthorOn: aCanvas of: aResponse ]
]

{ #category : #rendering }
PostComponent >> renderResponsesOn: aCanvas [
	post respuestas
		do: [ :resp | 
			aCanvas div
				id: 'responses';
				class: 'responses-container';
				with: [ self renderQuestionResponseOn: aCanvas response: resp ].
				aCanvas horizontalRule. ].
	

]
