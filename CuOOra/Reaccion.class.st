"
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design:

For the Class part:  State a one line summary. For example, ""I represent a paragraph of text"".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: Satate my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	estado:		<Object>
	fecha:		<Object>
	usuario:		<Object>


    Implementation Points
"
Class {
	#name : #Reaccion,
	#superclass : #Object,
	#instVars : [
		'fecha',
		'usuario',
		'estado'
	],
	#category : #'CuOOra-Model'
}

{ #category : #'as yet unclassified' }
Reaccion class >> nuevoConEstado: unEstado usuario: unUsuario [
	^ self new inicializarConEstado: unEstado  usuario: unUsuario 
]

{ #category : #public }
Reaccion >> esDeUsuario: unUsuario [
	^ usuario = unUsuario 
	
]

{ #category : #accessing }
Reaccion >> estado [
	^ estado
]

{ #category : #accessing }
Reaccion >> estado: anObject [
	estado := anObject
]

{ #category : #accessing }
Reaccion >> fechaYHora [
	^ fecha
]

{ #category : #accessing }
Reaccion >> fechaYHora: anObject [
	fecha := anObject
]

{ #category : #initialization }
Reaccion >> inicializarConEstado: unEstado usuario: unUsuario [
	estado := unEstado.
	usuario := unUsuario
]

{ #category : #initialization }
Reaccion >> initialize [
	fecha := DateAndTime now.
	estado := false.
	usuario := 'desconocido'
]

{ #category : #accessing }
Reaccion >> usuario [
	^ usuario
]

{ #category : #accessing }
Reaccion >> usuario: anObject [
	usuario := anObject
]
