Class {
	#name : #PreguntaTest,
	#superclass : #TestCase,
	#instVars : [
		'respuestas',
		'pregunta',
		'u1',
		'u2',
		'u3',
		'r1',
		'r2',
		'r3',
		'topicos',
		'topicos1',
		'topicos2'
	],
	#category : #'CuOOra-Test'
}

{ #category : #running }
PreguntaTest >> setUp [
	topicos := (Topico nuevoConNombre: '1' descripcion: '1')
		asOrderedCollection.
	topicos1 := OrderedCollection new.
	topicos2 := OrderedCollection new.
	topicos1
		add: (Topico nuevoConNombre: '2' descripcion: '2');
		add: (Topico nuevoConNombre: '1' descripcion: '1').
	topicos2
		add: (Topico nuevoConNombre: '3' descripcion: '3');
		add: (Topico nuevoConNombre: '4' descripcion: '4').
	u1 := Usuario nuevoConUserName: 'u1' password: '1'.
	pregunta := Pregunta
		nuevoConTitulo: 'hola'
		autor: u1
		cuerpo: 'asd'
		topicos: topicos.
	u2 := Usuario nuevoConUserName: 'u2' password: '2'.
	u3 := Usuario nuevoConUserName: 'u3' password: '3'.
	r1 := Respuesta nuevoConCuerpo: 'aaaaa' autor: 'a'.
	r2 := Respuesta nuevoConCuerpo: 'bbbbb' autor: 'b'.
	r3 := Respuesta nuevoConCuerpo: 'ccccc' autor: 'c'.
	r1
		darLike: u1;
		darLike: u3;
		darLike: u2;
		darDislike: u3.
	r2 darDislike: u2.
	r3
		darLike: u1;
		darDislike: u2.
	pregunta
		agregarRespuesta: r1;
		agregarRespuesta: r2;
		agregarRespuesta: r3.
	u1 agregarTopicosDeInteres: topicos1
]

{ #category : #tests }
PreguntaTest >> testAgregarRespuesta [
	| pregunta1 cant|
	pregunta1 := Pregunta nuevoConCuerpo: 'Test' autor: u1.
	cant:= pregunta1 respuestas size.
	pregunta1 agregarRespuesta: r1.
	self deny: cant equals: pregunta1 respuestas size.
	self assert: (pregunta1 respuestas includes: r1)
	
	
]

{ #category : #tests }
PreguntaTest >> testAgregarTopico [ 
	|preg cant topic|
	preg := Pregunta nuevoConTitulo: 'Test' autor: u1 cuerpo: 'asd' topicos: topicos .
	cant := preg topicos size.
	topic := Topico nuevoConNombre: 123 descripcion: 123.
	preg agregarTopico: topic.
	self deny: cant equals: preg topicos size.
	self assert: (preg topicos includes: topic)
]

{ #category : #tests }
PreguntaTest >> testCantidadDeRespuestas [
|pregunta2|
pregunta2:= Pregunta
		nuevoConTitulo: 'hola'
		autor: u2
		cuerpo: 'asd'
		topicos: topicos.
self assert: pregunta cantidadDeRespuestas equals: 3.
self assert: pregunta2 cantidadDeRespuestas equals: 0
]

{ #category : #tests }
PreguntaTest >> testChequearTopicos [
	self assert: (pregunta chequearTopicos: topicos1).
	self deny: (pregunta chequearTopicos: topicos2)
]

{ #category : #tests }
PreguntaTest >> testCompararCon [
	self assert: (pregunta compararCon: u1).
	self deny: (pregunta compararCon: u2)
	
]

{ #category : #tests }
PreguntaTest >> testCrearRespuestaConCuerpoYAutor [
]

{ #category : #tests }
PreguntaTest >> testObtenerPreguntasRelevantesParaUsuario [
	self assert: (pregunta obtenerPreguntasRelevantesParaUsuario: u1).
	self deny: (pregunta obtenerPreguntasRelevantesParaUsuario: u2)
]

{ #category : #tests }
PreguntaTest >> testObtenerRespuestasOrdenadasDeMayorAMenor [
	respuestas := OrderedCollection with: r1 with: r3 with: r2.
	self
		assert: pregunta obtenerRespuestasOrdenadasDeMayorAMenor equals: respuestas
]

{ #category : #tests }
PreguntaTest >> testTopicoDisponible [
	self deny: (pregunta topicoDisponible: (topicos at: 1)).
	self assert: (pregunta topicoDisponible: (topicos2 at: 1))
]
