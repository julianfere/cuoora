Class {
	#name : #CuOOraTest,
	#superclass : #TestCase,
	#instVars : [
		'cuoora',
		'preguntas',
		'p1',
		'p2',
		'p3',
		'u1',
		'r1',
		'topico1',
		'topico2',
		'topicos1',
		'topicos2',
		'topicos',
		'u2'
	],
	#category : #'CuOOra-Test'
}

{ #category : #running }
CuOOraTest >> setUp [
	topicos := OrderedCollection new.
	topicos add: (Topico nuevoConNombre: '1' descripcion: '1').
	u1 := (Usuario nuevoConUserName: 1 password: 2)
		agregarTopicosDeInteres: topicos.
	p1 := Pregunta
		nuevoConTitulo: 'preg1'
		autor: u1
		cuerpo: 'asd'
		topicos: topicos.
	topicos removeAll.
	topicos add: (Topico nuevoConNombre: '2' descripcion: '2').
	u2 := (Usuario nuevoConUserName: 2 password: 3)
		agregarTopicosDeInteres: topicos.
	p2 := Pregunta
		nuevoConTitulo: 'preg2'
		autor: u2
		cuerpo: 'preg2'
		topicos: topicos.
	u1 agregarPregunta: p1.
	u2 agregarPregunta: p2.
	preguntas := OrderedCollection new.
	preguntas
		add: p1;
		add: p2.
	cuoora := CuOOra nuevoConPreguntas: preguntas.
	cuoora agregarTopico: (Topico nuevoConNombre: '1' descripcion: '1').
	cuoora agregarUsuario: u1.
]

{ #category : #tests }
CuOOraTest >> testAgregarPregunta [
	cuoora
		crearPreguntaConTitulo: 'Test'
		autor: u1
		cuerpo: 2
		topicos: topicos.
	self assert: cuoora preguntas size equals: 3.
	"Es equals 3 porque en el set up se agregan 2 preguntras"
]

{ #category : #tests }
CuOOraTest >> testAgregarTopico [
	| cant topico|
	cant:= cuoora cantidadDeTopicos .
	topico := Topico nuevoConNombre: 'asd' descripcion: 'dasdas'.
	cuoora agregarTopico: topico.
	self deny: cant equals: cuoora cantidadDeTopicos.
	self assert: (cuoora topicos includes: topico)
	
]

{ #category : #tests }
CuOOraTest >> testAgregarUsuario [
	| cant usuario |
	cant := cuoora cantidadDeUsuarios.
	usuario := Usuario nuevoConUserName: 'test' password: 123.
	cuoora agregarUsuario: usuario.
	self deny: cant equals: cuoora cantidadDeUsuarios.
	self assert: (cuoora usuarios includes: usuario)
]

{ #category : #tests }
CuOOraTest >> testCantidadDeTopicos [
	self assert: (cuoora cantidadDeTopicos isKindOf: Integer).
	self assert: (cuoora cantidadDeTopicos)equals: 1.
]

{ #category : #tests }
CuOOraTest >> testCantidadDeUsuarios [
	self assert: (cuoora cantidadDeUsuarios isKindOf: Integer).
	self assert: cuoora cantidadDeUsuarios equals: 1
]

{ #category : #tests }
CuOOraTest >> testCantitadDePreguntas [
	self assert: (cuoora cantitadDePreguntas isKindOf: Integer).
	self assert: (cuoora cantitadDePreguntas) equals: 2
]

{ #category : #tests }
CuOOraTest >> testCrearPreguntaConTituloAutorCuerpoTopicos [
	|cant|
	cant := cuoora cantitadDePreguntas.
	cuoora crearPreguntaConTitulo: 'unTitulo' autor: u1 cuerpo: 'unCuerpo' topicos: topicos.
	self deny: cant equals: cuoora cantitadDePreguntas.
]

{ #category : #tests }
CuOOraTest >> testEliminarPregunta [
	| pregunta u|
	u := Usuario nuevoConUserName: 'Test' password: 'passTest'.
	pregunta := cuoora
		crearPreguntaConTitulo: 'Test'
		autor: u
		cuerpo: 123
		topicos: topicos.
	cuoora eliminarPregunta: pregunta.
	self assert: cuoora preguntas size equals: 2.
	self assert: u preguntas isEmpty
]

{ #category : #tests }
CuOOraTest >> testObtenerPreguntasDeTopico [
	|  unosTopicos |
	unosTopicos := (Topico nuevoConNombre: '1' descripcion: '1') asOrderedCollection .
	self
		assert: ((cuoora obtenerPreguntasDeTopicos: unosTopicos) at: 1)
		equals: p1
]

{ #category : #tests }
CuOOraTest >> testObtenerPreguntasDeTopicos [
	|preguntasFinales|
	preguntasFinales := cuoora obtenerPreguntasDeTopicos: topicos.
	self assert: (preguntasFinales includes: p2)
]

{ #category : #tests }
CuOOraTest >> testObtenerPreguntasDeUsuario [
	self assert: ((cuoora obtenerPreguntasDeUsuario: u1) at:1) equals: p1
]

{ #category : #tests }
CuOOraTest >> testObtenerPreguntasRelevantesParaUsuario [
	self
		assert: ((cuoora obtenerPreguntasRelevantesParaUsuario: u1) at: 1)
		equals: p1
		
]

{ #category : #tests }
CuOOraTest >> testObtenerUltimasCincoPreguntasRelevantesOrdenadasPorFecha [
	| u5 p3 p4 p5 p6 p7 p8 unosTopicos cuoora1 preguntas2 |
	unosTopicos := (Topico nuevoConNombre: '1' descripcion: '1')
		asOrderedCollection.
	u5 := (Usuario nuevoConUserName: 5 password: 2)
		agregarTopicosDeInteres: unosTopicos.
	p3 := Pregunta
		nuevoConTitulo: 'preg3'
		autor: u5
		cuerpo: 'asd'
		topicos: unosTopicos.
	p4 := Pregunta
		nuevoConTitulo: 'preg4'
		autor: u5
		cuerpo: 'asd'
		topicos: unosTopicos.
	p5 := Pregunta
		nuevoConTitulo: 'preg5'
		autor: u5
		cuerpo: 'asd'
		topicos: unosTopicos.
	p6 := Pregunta
		nuevoConTitulo: 'preg6'
		autor: u5
		cuerpo: 'asd'
		topicos: unosTopicos.
	p7 := Pregunta
		nuevoConTitulo: 'preg7'
		autor: u5
		cuerpo: 'asd'
		topicos: unosTopicos.
	p8 := Pregunta
		nuevoConTitulo: 'preg8'
		autor: u5
		cuerpo: 'asd'
		topicos: unosTopicos.
	u5
		agregarPregunta: p3;
		agregarPregunta: p4;
		agregarPregunta: p5;
		agregarPregunta: p6;
		agregarPregunta: p7;
		agregarPregunta: p8.
	preguntas2 := OrderedCollection new.
	preguntas2
		add: p3;
		add: p4;
		add: p5;
		add: p6;
		add: p7;
		add: p8.
	cuoora1 := CuOOra nuevoConPreguntas: preguntas2.
	self
		assert:
			(cuoora1
				obtenerUltimasCincoPreguntasRelevantesOrdenadasPorFecha: (cuoora1 obtenerPreguntasRelevantesParaUsuario: u5))
				size
		equals: 5
	"El ordenamiento no lo testee por que el sistema agrega todas las preguntas al mismo tiempo cuando se realizar una carga masiva y les pone a todas la misma fecha y hora exacta. Pero esta comprobado utilizando el sistema desde la UI que realemnte las ordena por fecha y hora de mayor a menor"
]

{ #category : #tests }
CuOOraTest >> testOrdenarPorFechaDeMayorAMenor [
	|p1t p2t|
	p1t := preguntas first.
	p2t := preguntas last.
	self assert: p1t fechaYHora < p2t fechaYHora 
]

{ #category : #tests }
CuOOraTest >> testRegistrarTopicoConNombreDescripcion [
	|cantTopicos|
	cantTopicos := cuoora cantidadDeTopicos .
	cuoora registrarTopicoConNombre: 'Test' descripcion: 'Testing'.
	self deny: cantTopicos equals: cuoora cantidadDeTopicos 
]

{ #category : #tests }
CuOOraTest >> testResponder [
	p1 crearRespuestaConCuerpo: 'Test'  autor: u1.
	self assert: u1 respuestas size equals: 1.
]

{ #category : #tests }
CuOOraTest >> testUsuarioConNombreProtegidoPor [
	self assert: u1 equals: (cuoora usuarioConNombre: 1 protegidoPor: 2)
	
]

{ #category : #tests }
CuOOraTest >> testVerificarTopico [
	| sistema topic1 |
	sistema := CuOOra new.
	topic1 := Topico nuevoConNombre: 'a' descripcion: 'a'.
	sistema agregarTopico: topic1.
	self assert: (sistema verificarTopico: 'a') equals: topic1
]
