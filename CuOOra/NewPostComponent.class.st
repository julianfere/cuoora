"
componente para nuevo post
"
Class {
	#name : #NewPostComponent,
	#superclass : #WebComponent,
	#instVars : [
		'postText',
		'postTitle',
		'postTopics'
	],
	#category : #'CuOOra-UI'
}

{ #category : #adding }
NewPostComponent >> cancel [
	postText := nil.
	postTitle := nil.
	postTopics := nil.
	self call: HomeComponent new
]

{ #category : #initialization }
NewPostComponent >> initialize [
	super initialize.
]

{ #category : #accessing }
NewPostComponent >> postText [
	^ postText
]

{ #category : #accessing }
NewPostComponent >> postText: anObject [
	postText := anObject
]

{ #category : #accessing }
NewPostComponent >> postTitle [
	^ postTitle
]

{ #category : #accessing }
NewPostComponent >> postTitle: anObject [
	postTitle := anObject
]

{ #category : #accessing }
NewPostComponent >> postTopics [
	^ postTopics
]

{ #category : #accessing }
NewPostComponent >> postTopics: anObject [
	postTopics := anObject
]

{ #category : #removing }
NewPostComponent >> removeTopic: aTopic [
	self postTopics remove: aTopic
]

{ #category : #rendering }
NewPostComponent >> renderContentOn: aCanvas [
	self renderHeaderComponentOn: aCanvas.
	aCanvas heading: 'Nueva Pregunta'.
	aCanvas horizontalRule.
	aCanvas
		form: [ aCanvas
				paragraph: [ aCanvas heading: 'Titulo'.
					(aCanvas textArea on: #postTitle of: self)
						columns: 80;
						rows: 2;
						placeholder: 'Escriba aqui el titulo de su pregunta' ].
			aCanvas
				paragraph: [ aCanvas heading: 'Desarrollo'.
					(aCanvas textArea on: #postText of: self)
						columns: 80;
						rows: 5;
						placeholder: 'Escriba aqui el desarrollo de su pregunta' ].
			aCanvas
				paragraph: [ aCanvas heading: 'Topicos'.
					(aCanvas textArea on: #postTopics of: self)
						columns: 80;
						rows: 2;
						placeholder: 'Ingrese topicos separados por coma' ].
			aCanvas
				paragraph: [ aCanvas submitButton
						callback: [ self save ];
						with: 'Guardar' ].
			aCanvas button
				callback: [ self cancel ];
				with: 'Cancelar' ]
]

{ #category : #adding }
NewPostComponent >> save [
	postTitle ifEmpty: [ ^ self ].
	postText  ifEmpty: [ ^ self ].
	postTopics ifEmpty: [ ^ self ].
	self model
		crearPreguntaConTitulo: postTitle
		autor: self session user
		cuerpo: postText
		topicos: (postTopics substrings: ',').
	^ self answer
]
