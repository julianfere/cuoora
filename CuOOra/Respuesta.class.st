"
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design:

For the Class part:  State a one line summary. For example, ""I represent a paragraph of text"".
a
For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.


    Implementation Points
"
Class {
	#name : #Respuesta,
	#superclass : #Publicacion,
	#category : #'CuOOra-Model'
}

{ #category : #'as yet unclassified' }
Respuesta class >> nuevaConCuerpo: unCuerpo autor: unAutor [
	^ (self nuevoConCuerpo: unCuerpo  autor: unAutor  )
	
]

{ #category : #public }
Respuesta >> calcularPuntaje [
	^ self calcularDiferenciaEntreLikesYDislikes + self puntajeBase
]

{ #category : #'add-remove' }
Respuesta >> eliminarDeUsuario [
	autor eliminarRespuesta: self
]

{ #category : #initialization }
Respuesta >> initialize [ 
	super initialize 
	
]

{ #category : #'as yet unclassified' }
Respuesta >> puntajeBase [
^ 50
]

{ #category : #'as yet unclassified' }
Respuesta >> verificarAutor: unUsuario [
^ autor == unUsuario.
]
