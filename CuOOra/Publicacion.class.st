"
Modelo de publicacion
"
Class {
	#name : #Publicacion,
	#superclass : #Object,
	#instVars : [
		'reacciones',
		'autor',
		'cuerpo',
		'fecha'
	],
	#category : #'CuOOra-Model'
}

{ #category : #'as yet unclassified' }
Publicacion class >> nuevoConCuerpo: unCuerpo autor: unAutor [
	^ self new inicializarConCuerpo: unCuerpo autor: unAutor
]

{ #category : #'add-remove' }
Publicacion >> agregarReaccion: unEstado usuario: unUsuario [
	reacciones
		add: (Reaccion nuevoConEstado: unEstado usuario: unUsuario).
]

{ #category : #accessing }
Publicacion >> autor [
	^ autor
]

{ #category : #calculations }
Publicacion >> calcularDiferenciaEntreLikesYDislikes [
	^ self cantidadDeLikes - self cantidadDeDislikes
	
	"^ reacciones sumNumbers: [ :each | each valorEstado ]"
]

{ #category : #accessing }
Publicacion >> cambiarEstadoDeUsuario: unUsuario estado: unEstado [
	| reaccion |
	reaccion := reacciones detect: [ :reac | reac esDeUsuario: unUsuario  ].
	"unEstado es para distinguir si se llama por darLike o darDislike
	si da distinto true es porque son distintas entonces tengo que cambiarlo
	si da false es porque on iguales entonces tengo que eliminarlo nada mas"
	self quitarReaccion: reaccion.
	(reaccion estado xor: unEstado)
		ifFalse: [ ^ self ].
	self agregarReaccion: unEstado usuario: unUsuario
]

{ #category : #calculations }
Publicacion >> cantidadDeDislikes [
	^ (reacciones reject: [ :r | r estado ]) size
]

{ #category : #calculations }
Publicacion >> cantidadDeLikes [
	^ (reacciones select: [ :r | r estado ]) size
]

{ #category : #accessing }
Publicacion >> cuerpo [
	^ cuerpo
]

{ #category : #accessing }
Publicacion >> cuerpo: anObject [
	cuerpo := anObject
]

{ #category : #accessing }
Publicacion >> darDislike: unUsuario [
	(reacciones anySatisfy: [ :reaccion | reaccion usuario = unUsuario ])
		ifFalse: [ self agregarReaccion: false usuario: unUsuario ]
		ifTrue: [ self cambiarEstadoDeUsuario: unUsuario  estado: false ]
]

{ #category : #accessing }
Publicacion >> darLike: unUsuario [
	(reacciones anySatisfy: [ :reaccion | reaccion usuario = unUsuario ])
		ifFalse: [ self agregarReaccion: true usuario: unUsuario ]
		ifTrue: [ self cambiarEstadoDeUsuario: unUsuario estado: true ]
]

{ #category : #accessing }
Publicacion >> fechaYHora [
	^ fecha
]

{ #category : #'add-remove' }
Publicacion >> inicializarConCuerpo: unCuerpo autor: unAutor [
	cuerpo := unCuerpo.
	autor := unAutor
]

{ #category : #initialization }
Publicacion >> initialize [
	fecha := DateAndTime now.
	reacciones := OrderedCollection new.
]

{ #category : #accessing }
Publicacion >> nombreAutor [
	^ autor userName 
]

{ #category : #'add-remove' }
Publicacion >> quitarReaccion: unaReaccion [
	reacciones remove: unaReaccion .
]

{ #category : #accessing }
Publicacion >> reacciones [
	^ reacciones
]
