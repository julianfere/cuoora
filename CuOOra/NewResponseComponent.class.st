"
componente para nueva respuesta
"
Class {
	#name : #NewResponseComponent,
	#superclass : #WebComponent,
	#instVars : [
		'cuerpoInput',
		'post'
	],
	#category : #'CuOOra-UI'
}

{ #category : #'as yet unclassified' }
NewResponseComponent class >> nuevoConPost: aPost [
	^ self new post: aPost
	
]

{ #category : #actions }
NewResponseComponent >> cancel [
cuerpoInput:= nil.
self answer
]

{ #category : #accessing }
NewResponseComponent >> cuerpo [
	^ cuerpoInput
]

{ #category : #accessing }
NewResponseComponent >> cuerpo: anObject [
	cuerpoInput := anObject
]

{ #category : #creation }
NewResponseComponent >> makeResponse: aCanvas [
	aCanvas div
		class: 'response-form';
		with: [ aCanvas text: 'RESPONDER '.
			aCanvas
				form: [ (aCanvas textArea on: #cuerpo of: self)
						columns: 80;
						rows: 12;
						placeholder: 'Escriba aqui el texto de su respuesta'.
					aCanvas div
						class: 'submit-options';
						with: [ aCanvas div
								class: 'submit-item';
								with: [ aCanvas button
										callback: [ self cancel ];
										with: 'Cancelar'.
									aCanvas submitButton
										callback: [ self respond ];
										with: 'Guardar' ] ] ] ]
]

{ #category : #accessing }
NewResponseComponent >> post [
	^ post
]

{ #category : #accessing }
NewResponseComponent >> post: anObject [
	post := anObject
]

{ #category : #rendering }
NewResponseComponent >> renderContentOn: aCanvas [
	self renderHeaderComponentOn: aCanvas.
	self renderQuestionWithoutDetail: post on: aCanvas.
	aCanvas horizontalRule.
	self makeResponse: aCanvas
]

{ #category : #actions }
NewResponseComponent >> respond [
	cuerpoInput ifEmpty: [ ^ self ].
	post crearRespuestaConCuerpo: cuerpoInput autor: self session user.
	self answer
]
