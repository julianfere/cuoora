"
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design:

For the Class part:  State a one line summary. For example, ""I represent a paragraph of text"".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two a
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	email:		<Object>
	password:		<Object>


    Implementation Points
"
Class {
	#name : #AccessGrantingComponent,
	#superclass : #WebComponent,
	#instVars : [
		'name',
		'password'
	],
	#category : #'CuOOra-UI'
}

{ #category : #accessing }
AccessGrantingComponent >> model [ 
	^ CuOOra soleInstance 
]

{ #category : #accessing }
AccessGrantingComponent >> name [
	^ name
]

{ #category : #accessing }
AccessGrantingComponent >> name: anObject [
	name := anObject
]

{ #category : #accessing }
AccessGrantingComponent >> password [
	^ password
]

{ #category : #accessing }
AccessGrantingComponent >> password: anObject [
	password := anObject
]

{ #category : #'as yet unclassified' }
AccessGrantingComponent >> proceedWith: aUser [
	self session user: aUser.
	self call: HomeComponent new
]

{ #category : #rendering }
AccessGrantingComponent >> renderButtonsOn: aCanvas [
	self subclassResponsibility
]

{ #category : #rendering }
AccessGrantingComponent >> renderContentOn: aCanvas [
	aCanvas heading: 'CuOOra'.
	aCanvas break.
	aCanvas
		form: [ self renderInputElementsOn: aCanvas.
			self renderButtonsOn: aCanvas ]
]

{ #category : #rendering }
AccessGrantingComponent >> renderInputElementsOn: aCanvas [
	aCanvas label: 'Usuario'.
	aCanvas paragraph: [ aCanvas textInput on: #name of: self ].
	aCanvas label: 'Contraseña'.
	aCanvas paragraph: [ aCanvas passwordInput on: #password of: self ]
]
