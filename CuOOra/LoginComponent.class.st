"
| application |
application := WAAdmin register: LoginComponent asApplicationAt: 'simple-net'.
application sessionClass: SessionWithUser.
"
Class {
	#name : #LoginComponent,
	#superclass : #AccessGrantingComponent,
	#category : #'CuOOra-UI'
}

{ #category : #'private protocol' }
LoginComponent >> login [
	(self model usuarioConNombre: name protegidoPor: password)
		ifNotNil: [ :it | self proceedWith: it ].
	name := nil.
	password := nil
]

{ #category : #'as yet unclassified' }
LoginComponent >> registerNewUser [
	self call: RegisterComponent new 
]

{ #category : #'as yet unclassified' }
LoginComponent >> renderButtonsOn: aCanvas [
	self renderSignInButtonOn: aCanvas.
	aCanvas horizontalRule.
	aCanvas paragraph: 'Crear nueva cuenta'.
	self renderSignUpButtonOn: aCanvas
]

{ #category : #rendering }
LoginComponent >> renderSignInButtonOn: aCanvas [
	aCanvas submitButton
		callback: [ self login ];
		with: 'Iniciar Sesión'
]

{ #category : #rendering }
LoginComponent >> renderSignUpButtonOn: aCanvas [
	aCanvas submitButton
		callback: [ self registerNewUser ];
		with: 'Registrar'
]
