"
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design:
a
For the Class part:  State a one line summary. For example, ""I represent a paragraph of text"".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.


    Implementation Points
"
Class {
	#name : #Pregunta,
	#superclass : #Publicacion,
	#instVars : [
		'titulo',
		'topicos',
		'respuestas'
	],
	#category : #'CuOOra-Model'
}

{ #category : #'as yet unclassified' }
Pregunta class >> nuevoConTitulo: unTitulo autor: unAutor cuerpo: unCuerpo topicos: unosTopicos [
	^ (self nuevoConCuerpo: unCuerpo autor: unAutor)
		inicializarConTitulo: unTitulo
		topicos: unosTopicos
]

{ #category : #'add-remove' }
Pregunta >> agregarRespuesta: unaRespuesta [
	respuestas add: unaRespuesta
]

{ #category : #'add-remove' }
Pregunta >> agregarTopico: unTopico [
	topicos add: unTopico 
]

{ #category : #public }
Pregunta >> calcularPuntaje [
	^ self calcularDiferenciaEntreLikesYDislikes + self puntajeBase
]

{ #category : #public }
Pregunta >> cantidadDeRespuestas [
	^ (respuestas size)
]

{ #category : #comparing }
Pregunta >> chequearTopicos: unosTopicos [
	^ topicos anySatisfy: [ :each | each compararCon: unosTopicos ]
]

{ #category : #comparing }
Pregunta >> compararCon: unUsuario [
^ autor = unUsuario
]

{ #category : #creating }
Pregunta >> crearRespuestaConCuerpo: unCuerpo autor: unAutor [
	| respuesta |
	respuesta := Respuesta nuevoConCuerpo: unCuerpo autor: unAutor.
	self agregarRespuesta: respuesta.
	unAutor agregarRespuesta: respuesta
]

{ #category : #'add-remove' }
Pregunta >> eliminar [
	autor eliminarPregunta: self.
	self  eliminarRespuestas
]

{ #category : #'add-remove' }
Pregunta >> eliminarRespuestas [
	respuestas do: [ :respuesta | respuesta eliminarDeUsuario ].
	respuestas := OrderedCollection new.
	"Nose que tan bien este esto"
]

{ #category : #initialization }
Pregunta >> inicializarConTitulo: unTitulo topicos: unosTopicos [
	titulo := unTitulo.
	topicos addAll: unosTopicos
]

{ #category : #initialization }
Pregunta >> initialize [
	super initialize.
	respuestas := OrderedCollection new.
	topicos := OrderedCollection new
]

{ #category : #accessing }
Pregunta >> obtenerPreguntasRelevantesParaUsuario: unUsuario [
	^ (self chequearTopicos: unUsuario topicos) or: (unUsuario chequearSeguidosConAutor: autor)
]

{ #category : #public }
Pregunta >> obtenerRespuestasOrdenadasDeMayorAMenor [
	^ respuestas
		sort: [ :res1 :res2 | 
			res1 calcularDiferenciaEntreLikesYDislikes
				>= res2 calcularDiferenciaEntreLikesYDislikes ]
]

{ #category : #'as yet unclassified' }
Pregunta >> puntajeBase [
^ 20
]

{ #category : #accessing }
Pregunta >> respuestas [
	^ respuestas
]

{ #category : #accessing }
Pregunta >> titulo [
	^ titulo
]

{ #category : #accessing }
Pregunta >> titulo: anObject [
	titulo := anObject
]

{ #category : #comparing }
Pregunta >> topicoDisponible: unTopico [
	(topicos includes: unTopico)
		ifTrue: [ ^ false ].
	^ true
]

{ #category : #accessing }
Pregunta >> topicos [
	^ topicos
]
