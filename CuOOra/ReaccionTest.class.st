Class {
	#name : #ReaccionTest,
	#superclass : #TestCase,
	#instVars : [
		'u1',
		'u2',
		'reaccion'
	],
	#category : #'CuOOra-Test'
}

{ #category : #running }
ReaccionTest >> setUp [
	u1 := Usuario nuevoConUserName: 'Test' password: 123.
	u2 := Usuario nuevoConUserName: 'Test2' password: 123.
	reaccion := Reaccion nuevoConEstado: true usuario: u1
]

{ #category : #tests }
ReaccionTest >> testEsDeUsuario [
	self assert: (reaccion esDeUsuario: u1 ).
	self deny: (reaccion esDeUsuario: u2)
	
]
